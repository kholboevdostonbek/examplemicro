package main

import (
	"fmt"

	"gitlab.com/micro/examplemicro/handler"
	pb "gitlab.com/micro/examplemicro/proto"

	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/config"
	"github.com/micro/micro/v3/service/logger"
)

func main() {
	// Create service
	srv := service.New(
		service.Name("examplemicro"),
		service.Version("latest"),
	)

	// read config value
	val, _ := config.Get("key.subkey")
	fmt.Println("Value of key.subkey: ", val.String(""))

	// Register handler
	pb.RegisterHelloworldHandler(srv.Server(), new(handler.Helloworld))

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
